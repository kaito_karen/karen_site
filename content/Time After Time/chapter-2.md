---
title: "Chapter 2"
date: 2020-01-14T18:21:53-08:00
---

After breakfast, I took Hideyoshi out to shopping downtown. I lived by myself, so there were no male clothes for him to wear. Except his original Japanese warlord kimono. Good thing was nobody else here judged, unlike in Asia. People here were passionate about themselves, and they respected others' rights to do the same. That was the one thing so great here - freedom.

However, considering how hot the summer was, I did not think keep wearing his original layered outfit would do him any good.

"What are those?" Hideyoshi pointed at a car passing by. It was a black one, might have been passing the speed limit a bit. 

"That is a car. People here commute by driving, or by public transportation. Just like horses in your time."

"They move incredibly fast." He noted. "Wouldn't it be dangerous to ride in that?"

"Quite. People get into car accidents now and then." I replied, noticing how he was more concerned about safety. If it was Masamune, he would probably have jumped on top of a car by now. "Don't worry, we're taking a safer one. It's called a bus. The drivers are very careful, and there will be many people."

"Interesting. Your town is a lot different from Azuchi. The houses here are also weird, too."

"I wouldn't call it a town." I giggled. "This nation alone is already many times bigger than Japan already. From now on, be prepared to get mind-blown."

"Mind-blown?" Hideyoshi grimaced. "I don't think I like the sound of that."

I laughed at his reaction. Teasing him was fun, but the bus had arrived, so I tugged the sleeve of the confused Hideyoshi and pulled him on the bus.

"What is this notorious monster??"

Everyone on the bus turned around to look at us, as soon as Hideyoshi finished his question. 

"Sshhh, it's just a vehicle." I whispered to him and turned to everyone else. "It's nothing, he's just too deep into his character."

The bus was crowded as always, and there were barely any seats left. So we stood, Hideyoshi next to me. Only then did I notice how tall he was. His chest was wide and firm, which blocked all of my view if I turned around, because I was only as tall as his shoulders. 

"Is something the matter? You're not saying anything. You can sit down if you want, you don't have to stand up with me."

I glanced up, meeting the concerned look in his eyes.

"No, nothing is wrong." I hurriedly replied as the heat rushed to my face, realizing I had been staring for a while. "It's just a short ride. We are almost there."
-------------------

Once the bus reached the Asian corner - a combination of Chinatown, Little Tokyo and even more, I led Hideyoshi to a clothing shop. I could have taken him to a normal American mall, but I wasn't so sure if Western fashion would suit a historical Japanese warlord. The other reason was I had always dreamt of seeing one of the IkeSen guys dressed in Korean oppa style. 

Unsurprisingly, Hideyoshi looked good in any outfit he put on. I had to resist the urge to just pull my phone out and take several pictures of him, lest the staff be creeped out. In the end, I picked some simple shirt of black and white, and two other jackets. One of them was in green army color, and the other in blue navy khaki. I also picked up some pants, formal and informal, some emitted the aura of a good and caring big brother, but some others had rips and chains all over it. Hideyoshi seemed to have left the entire decision to me, as he nodded with every question I asked. He said these clothes were too revealing, and didn't look safe if someone else pointed a sword at him. I assured him that there would be no one here interested in doing that.
-----------------
At the end, I got carried away and my wallet had to suffer. I bought a total of five big bags, which he happily carried them for me, and the staffs beamed at us as they waved us goodbye. 

"You didn't need to buy so many things for me, Arisu." He looked troubled. "I don't even know how to repay you yet."

"Don't worry about it." I patted his hand. "I know you would've done the same thing for me. You don't owe me anything."

How could he say that he wouldn't be able to repay me, when he already had? I still remembered clearly, that it had been Hideyoshi's route that had come to me during one of my breakdowns. It was all thanks to him that I could move on, to continue living and trying.

I took him to a coffee shop next, only to get two take-away of Frappuccino. After that, we walked down the streets of downtown together, slipping the bitter-sweet coffee from Starbucks. It was a summer day alright, but we got some breeze. I just thought it would be fun to simply walk with him, around the modern city.

“You are right, everything here is big. All these houses are so high. Would they fall one day?”

“Those are skyscrapers. There are not that many tall buildings in the city, but since we are in the financial district right now, you’ll see a lot of those.” I pointed at one of them, where business people with suit and tie constantly walked in and out. “There are people working inside. To manage this city’s finance and invest in many more. My major is Finance. One day, I wish to be working in one of those.”

I wasn’t even sure if that was truly my wish, or if it was something my family imposed on me. Nowadays, my wishes had faded, leaving space only enough for society’s expectation to fill in. 

“You’re great, aren’t ya?” Hideyoshi suddenly left out a laugh. “I could barely understand what you’ve just said. It sounds complicated. Don’t people farm anymore? Back in Azuchi, farming and trading were what pushed the economy.”

“Essentially, yes.” I gazed up, lost in the knowledge I got from university classes. “Some people do, but now farming is mostly done by machines. It saves human resources and reduces the need to pay people. Machines are now more efficient, that’s why the economy can be pushed up without having so many people doing labor. This leaves room for more investment, and that’s where people like those guys in front of us come in. They invest in the stock market, invest in corporation, lend other people money, so that more companies can grow and benefit the economy. It’s a system where everyone benefits each other, or at least that’s what it’s supposed to be.”

“You make it sounds as if no one has to worry for their lives and well-beings.” Hideyoshi commented. “And all that they care about is to grow the economy.”

“Well, to business major like me, yes, that is our job at the end of the day.” I turned back to him. “Not everyone is like that. But you’re right, we don’t have to worry about whether we will live or die tomorrow. We just need to think of how to make a living, so we can afford paying our rents and other needs.”

“Would such a world really exist?” His eyes were wide open, as if he thought I was telling him a fairy tale. 

“You’re in such a world right now.” I laughed, unable to hold my excitement in. My voice might have been a little louder than usual. “Hideyoshi, this IS the world that Nobunaga was seeking. This IS a world without war, where people are recognized in equality, there is no status or power, there is no fighting, there is no slaughtering, at least not unjustly. In here, you can be ANYTHING that you want to be!”

He seemed to be taken aback for a few seconds, and during those few seconds, I thought I might have gone overboard. No, I definitely had, because some random hippie passing by was yelling, “YEAH, PREACH IT, GIRL!” at me.

Good thing was July the 4th was coming, so nobody else thought I was crazy. Still, I was blushing like mad.

“Unbelievable.” Hideyoshi’s soft murmur caught my ear. His voice got louder as what I said finally sank in. “Are you telling the truth? That there is no need for war in this world?? Is that why they are able to build such high buildings?”

“Well, part of it…” I hesitated. “We are living in peace, at least for the moment. There are still tensions on the world between certain nations, but the majority of us are lucky enough to enjoy peace every day.”

“Incredible!” He replied, more like an echo of his astonishment.

“And that’s why I told you to leave your sword back home. There will be no need for you to use them. In fact, carrying a weapon around in public is not allowed, I think.”

“In Azuchi, samurai and soldiers carry their weapons to wherever they went. Even during Lord Nobunaga’s time, there are still the strong bullying the weak. No matter how hard the Oda tried, we cannot prevent them all.” 

At that moment, I glanced up at Hideyoshi, curious to see what kind of expression he would carry when talking about Azuchi. Even as I expected it, I was still surprised by the mixture of reminiscence and sadness yet filled with pride as he talked about his lord. 

“It wasn’t for nothing, you know.” I turned back to the road, my frappucino dripping wet from melting ice in my hand. “It was thanks to people like you and Nobunaga that we are able to enjoy this peace and freedom. It was thanks to people who shared his ideals that people like me don’t have to worry about our family dying tomorrow. As a future resident, I am grateful for all that you guys have done.”

The person walking beside me went silent. He didn’t say a word, but I could feel the air around me went quiet. I was too afraid to look up again, feared that looking at him would make me cry, feared to find out whether he felt pain or relieved upon hearing that. However, I said what I wanted to say, or what any normal person would say, if a famous historical person time-travelled to see them. 

*Thank you. Thank to all of you that we are able to live this beautiful life. Thank you for all of your sacrifices, all of your contributions, thank you for not losing faith in humanity.*

My feet suddenly felt like lead. My eyes felt drops of water gathering, on the verge of a downpour. ‘This is bad,’ I thought to myself, ‘even though I tried so hard not to get emotional—’

“Ha.”

A short laugh startled me. Was that Hideyoshi’s?

“Really, haha! You are amazing, Arisu.” It was his laugh. He was laughing, a reaction completely out of my imagination. “Indeed. How did you manage to do that?”

“Do what?” I replied, completely baffled. 

“How did you manage to wipe away all of the distorted emotions in my mind just with a few words?” He was still laughing, but stopped to look at me. “Here I was, thinking about how powerless I am, worried sick about Lord Nobunaga being back in Azuchi alone without his right hand, and how the enemy could strike at any moment. I started to have doubts in myself, wondering whether the path that I decided to follow is right, whether everything Lord Nobunaga is sacrificing will be worth it.”

He reached out his hand to pat my head, his coffee and all the shopping bags in the other hand. I usually hated it when guys did that, as the gesture always implied them mocking at my height disadvantage. Hideyoshi was different. His hand was so gentle, and the look he gave me was so kind that I couldn’t bring myself to pull back. Was this one of his spells?

“… but after listening to you, I realized that this is a light of hope to me. This is the end of the road. This is the world that Lord Nobunaga is looking for. I shouldn’t have any doubts in my lord, he will surely be fine without me by his side, but I do wish to come back to him and support him with my whole life. I’m sure he would be astounded to hear this wonderful news! Who knew that in the future, there would be a world like this? This is too good to be true!”

Anddddd the magic disappeared. Of course, it had to be Nobunaga. Nobunaga always came first in Hideyoshi’s head. For a moment, I forgot that in the beginning of the story, Hideyoshi would gladly throw his life away for his lord. It seemed that this thinking had not disappeared yet, even after he travelled to the future. 

“So that’s what happened. You were worried about Nobunaga.” I inhaled sharply. “Hideyoshi, he is called the Devil King of the 6th Heaven. He will be just fine.”

“You are so right.” He agreed with me right away. “Lord Nobunaga is a great man. There is no way he will falter with just one of his attendant disappearing!”

For a moment, I felt jealous, and even had the temptation to just stomp away, because I got worried for nothing. I even thought he was missing home, so I was concerned about Hideyoshi himself, not Nobunaga. Even so, Hideyoshi’s face beamed with enthusiasm when he spoke of Nobunaga. Though not with an angelic smile like Mitsunari’s, it wasn’t something that you could just get mad at, either. I would feel like a villain if I disturbed his streak of excitement right now. 

Therefore, I did not stop him, and Hideyoshi continued with his conversation about Nobunaga. We walked side by side, on the wide streets with people of different origins, different clothing, all there for different reasons. The world was moving around us, but Hideyoshi seemed to be unaware of that. He was lost in his thought, in his admiration for his lord, and went on and on about Nobunaga’s victories from battle to battle. I simply sipped my coffee next to him, feeling the sugar linger at the end of my tongue as I listened to his energetic voice. The pigeons chirped at us as we walked by them, not even bothered to hop away. He was much taller than me, but our pace would always match, no matter how deeply he was in the conversation. It was a strange morning, yet the most peaceful one I’d experienced lately. 








