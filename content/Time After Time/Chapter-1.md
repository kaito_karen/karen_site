---
title: "Chapter 1"
date: 2020-01-14T17:32:53-08:00
---
I am not your normal average girl. And by that, I don't mean that I have superpower, nor am I an excellent individual of society.

I just can't fit in anywhere.

At school, I am a nerd who wants to get absolute scores in every subject. I stay away from parties, sororities, clubs or any "fun" things that other kids do. I am a nervous wreck when it comes to communication, but I also have a high esteem that deems myself greater than anyone else. There are times I feel like I can conquer the world, but there are times I feel like a worthless human being. 

There is also one more thing that sets me out from the rest of my circle: I love otome games. I love shoujo mangas. I love reading love stories that are too good for real life. I had many past relationships, but they all ended shortly because those guys could never live up to all my 3D boyfriends. I'm a Virgo, so that makes me a perfectionist. My boyfriend should be someone who can take care of me, have confident in himself, yet not too much confidence that he would appear cocky, and must be cold to other girls but at the same time a sweetheart to me.

"Andddd you really think a guy like that exists." 

My best friend interrupted me cynically, raising her eyebrows through the phone screen. 

"Sorry to burst your bubbles, honey, but this is America. Guys want to have sex with you on the first date, and they will dump you afterward! Keep holding onto those traditional Asian values won't get you anywhere!"

Oh, have I told you I'm from Southeast Asia? Where people blush just from holding hands, and kissing is a sacred thing?

My bestie wasn't wrong - this is America, and people don't do that here.

"I might as well stay single for the rest of my life." I groaned.

"Oh really? I'll wait and see how long you can prolong this 'Single' status. Good night."

I waved at my screen as the video call ended. 

I was going through a somewhat mild depression phrase at that time. My best friend who studied in the East coast was diagnosed with moderate depression, which was serious enough that she had to cut off from all social media accounts, friends, and even schoolwork. We were both attending universities, and with the pressure from our families half a planet away, along with the solitude of not having anyone who could understand and share what we were going through, put us at the verge of abhorring life any moment.

Letting out a long sigh, I turned off the lights and opened the dating game on mobile phone. At nights like these, when I had millions of questions about where my life had gone wrong, the game always managed to soothe me and bring me a good night sleep.

Whatever. Who needed a real guy, when she could just hang out with fictional handsome characters?
------------------

The next morning, I woke up to a loud thud.

Is the cat sneaking into my room again? She'd better not be jumping up and down on my-

I swallowed as I see him on my floor. The face that I recognized too well. Why, I have been staring at that face for hours for the past few months, watching him move through my phone screen, spouting out romantic lines.

"You can't be-"

The person sitting on the floor wore a confused expression on his face, his hazelnut eyes stares back at me, his voice saying nothing yet made me felt like he had voice his many questions.

"Hideyoshi Toyotomi??"

Hideyoshi's eyes sparked a light of recognition, which immediately faded. 

"How did you know my name?" He spoke up, his voice even sweeter than that I had always heard through headphones.

"Where's Lord Nobunaga? What did you do to me? Where am I?" 

Seeing that I uttered no answer - where should I even begin? - he continued with the questions, sword ready to be drawn at anytime, and probably to be pointed at me.

"Please calm down first." I tried to keep my voice as normal as possible, hiding the nervousness. "I'm not here to harm you. I don't know where Nobunaga is, nor do I know why you appear in my room like this."

Or to be more precise, on my bed, before you rolled off and almost broke my floor.

He looked at me doubtfully. I raised my hands up, showing no sign of hostile. As if knowing better to get angry with a little girl, Hideyoshi gave in. He took a careful look around my room, judging every single item of its weirdness. 

“Is this still Japan?” 

I wish. 

“No, this is America. Half a globe away from Japan. Also, I believe it’s 500 years later into the future from your timeline.”

The truth might have been too hard. After seeing his abashed look, I regretted spouting this out right away.

“Sorry, Hideyoshi. This is a whole new world to you. I don’t know how you end up here, but I promise I would find a way to return you home.” I spoke, tried to find some reassurance for him. I knew too well the loneliness of being somewhere you’re not supposed to be, and the thought of putting him through that just broke my heart. 

Hideyoshi looked at me like he couldn’t believe what I’d just said. After a few seconds, he shook his head, and rose from the floor. 

“Don’t say that.” He rustled my hair, which taken me by surprise. “You’re just a girl. What kind of guy would I be if I let a lady take care of my well-being?”

I glanced up at him, through my disoriented bang. “Are you… not scared?” 

“Interesting question you have for me there.” He let out a laughter, which was deep and sweet like deadly dangerous honey. “Aren’t you scared of having a stranger in your room?”

Y-You are not a stranger!

I smiled at his question, holding my exclamation in. I’m too afraid to tell him about the game, that he was just a fictional character in my phone. Someday, maybe, I will tell him. But today was not the day.

“You don’t look like someone who would hurt people without a good reason.” I knew, because I’d heard of your past, and the reason why you were so loyal to Nobunaga. I had also seen your sweet side, there was no way I could have hated you after all that. 

“You trust people too much.” He shook his head. “But alright, since you’ve said that, you have my word that I will not hurt you, if you can help me to find out how I can return to my time.”

Acting tough, aren’t we? He tried to pose himself as someone bad, so that I would learn not to trust just any guy. 

“Deal.” I nodded. “I’m sorry that you met me. If it could have been someone else more capable, you probably would have a much faster way to return. I’m just useless here. But I will try my best.”

I purposefully left out the “I am not worthy of a human being, sorry for disappointing you”. I didn’t want the newcomer to feel depressed, or troubled that he couldn’t understand the definition of depression, or social anxiety and the likes.

“Never mind that. You must be hungry, right?” I hid my emotion behind a professional smile, before he had anytime to comprehend what I had just confessed. “I happen to have some eggs left in the fridge. Have you ever had fried egg for breakfast before? Let’s try it out!”

With that, I took his hand and led him down to the kitchen. His hand was big and warm, like that of my ex boyfriend, a blackbelt Aikido, but rougher. Must be tough being a warlord and all. Sengoku era was full of battle, constant bloodshed could break out at any time. In the game, it was not that bad, of course. CYBIRD is very sweet, they wouldn’t want their fans to experience any bad memories when they went inside the Sengoku period. Nonetheless, it didn’t change the fact that reality was brutal. Even in the game, we players couldn’t tell how bad it was in battle scenes. If only he could stay…

I snapped out of my thought as the sizzling of hot oil popped. The smell of egg filled the kitchen area. Hideyoshi is standing on the opposite side of the stove, staring at the burning-machine curiously. 

“Sorry, I got lost in my thought.” I hurriedly turn off the stove and put the eggs on two slices of bread on each plate. “I don’t usually have an appetite for breakfast, so tell me if you want more than this.”

“That is fine.” He took the plates and set them on the table. “This is an awfully high table. And why do these chairs move around?”

“That’s how they designed stuffs. It’s a trend here.” I shrugged. The apartment simply came like that, and I was too lazy to say things that would only trigger more questions.

Hideyoshi then quietly ate his breakfast. I thought he would have more questions, but he didn’t ask anything, even though it was all clear in his eyes. The furniture, the gadgets, the decorations, everything was unfamiliar to him. It was all too different. 

“That’s right, Hideyoshi. Would you like to see the city?” 

“Outside? This world?”

“Yes. I’m sure you’d love it.” I smiled in return. Seeing the city would be a great opportunity for him to see how the world has changed after 500 years. Having a hope that Nobunaga could change the world and make it a place without war was one thing, but seeing the dream materialized itself would be a whole other level.

“Sure. I have nothing else to do anyway. I might as well just use this time to find out what kind of world this is first.”

I nodded while taking his empty plate away to wash. Could there be 3 choices of answer for him to choose from, just like when I was playing the game? That can’t be, but it would have been an interesting theory for alternative universes.

“May I ask, what’s your name?” 

I startled at the hot breath in my ear. Hideyoshi was standing behind me, peeking his head over my shoulder. The intimacy made me blush.

“Wh-Why are you here?!”

“I thought you might need some help with the dishes.” He answered nonchalantly. “Though it appears that I wouldn’t know how to do it, so I thought I can start learning too.”

I stepped aside, leaving him some space to stand. Away from me. To keep my heartbeat at normal rate.

“My name is Alice.” `(to fellow IkeRev players: Yes, I just find it fun to mess up with these two MCs)`

“A… Arisu…?”

“No… Not that, A-lice.” I repeated. His Japanese accent was not helping. We were still communicating in English though, which came as a surprise and a mystery. 

“Arisu.” He tried again, but to no avail. “I thought that was what you said?”

“No, it’s not. Ugh… Fine, call me whatever you like.” 

“I’m Hideyoshi. Though I suppose you already know that.” He smiled. It was a heartwarming smile, even the sunlight in autumn would fade compared to his warmness. “I will be in your care, Arisu, until I can find a way back to Lord Nobunaga.”

Why do I feel like a third wheel here?

“It’s Alice.” I corrected him again, just to ignore the back-to-Nobunaga part. “I don’t have much, but I hope you will enjoy your time here.”





