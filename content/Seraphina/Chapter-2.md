---
title: "Chapter 2"
date: 2020-01-09T23:32:53-08:00
---

The maid led me down an extravagant hallway decorated with vintage paintings. The carpet felt soft and expensive, like those in European museums. There were vases with flowers adorning alongside of the hallway,of which I had never seen in my life.

When the maid was busy brushing my hair, I was able to recollect the information I known in my head. My guess was that I had died from the accident, and somehow woke up in this world in the body of Seraphina Celandine. Seraphina came from the royal family, but since her mother ended up falling in love and giving birth to a man from the commoner class, she was chased out from the palace. Due to her frail body, Seraphina’s mother passed away when Seraphina turned three, leaving her daughter in her husband’s household the countryside. 

One day, the royal ambassador of the crown prince suddenly sent words to Seraphina’s house, issuing an order that Seraphina moved to the royal palace promptly. Without an explanation, Seraphina was taken forcefully to this place, and in despair from not being able to escape, she attempted to kill herself with sleeping pills. 

A tragic life.

I shook my head. There was nothing I could do to bring her back. The girl was already long gone. I probably would not be able to return to my past life either, because I died there. 

“My lady, we have arrived.”

While I was lost in my thought, the maid had already led me to the dining room. As I walked in, two figures were waiting on the table inside.

----------

“You are awfully late.” 

A low and cold voice arose from the other side of the room. As I squinted my eyes to see who it was, a familiar face came to my vision.

“You are…” 

Alexander de Claus, the #1 love interest in the otome game I’ve just played!

I managed to swallow to latter part down, hand clamping over my mouth, eyes wide open. 

How is this possible?

The man looked annoyed at my reaction, but said nothing about it. He simply waved his hand and ordered the maid to show me to my seat.

I retracted my facial expression, and calmly followed the maid.

When I glanced to the other person in the room, I saw a girl looking at me with a worried look in her eyes. 

“Brother, you didn’t tell me there would be another guest joining us for breakfast.” She spoke with a quiet voice, her amber eyes scanning me all over. 

I recognized this person as well. She must be no other than the heroine, Elena Sylvatica, who was called here by her brother, Alexander de Claus, to this palace once she turned 16 years old. As usual, Elena is a heroine who captivated everyone’s heart and had a happy ending waiting for her at the end of every route. 

I was surprised at myself for accepting this reality as a game so easily. First of all, the whole isekai thing was already out of mind for me to comprehend. I must be going crazy now.

 “Elena, Seraphina is your cousin. You two will be living together in this palace from now on. I hope you can get along with each other.”

Alexander slowly announced, which put Elena to silence. She looked as if she wanted to protest, but did not dare voice it out. 

The atmosphere in the dining room was heavy. Alexander enjoyed his meal in silence, while Elena occasionally glanced my way. Figuring this would be the only chance I could see him, I decided to speak up.

“Your Highness, if I may ask a question…”

“Speak.” He cut me off.

“For what reason have you brought me here?”

I looked up at Alexander, keeping my gaze on him. His cold expression might have been enough to freeze the whole room, but the thought of him being just a character in a game I played nullify all those side effects. 

“Your place was originally here. My father simply made a mistake by chasing you and your mother out.” 

He gave me an answer that neither affirm or deny anything. An answer without any helpful information.

“I understand your need to strengthen the power of the royal palace, given the chaos in the Imperial Palace right now. But your highness, I assure you, I do not belong here. I already have a lover, and it makes me feel terrible breaking my promise with him.”

I continued with my plea. Consider this my attempt to follow the former owner of this body. Seraphina wanted to return to her hometown and to her lover, Felix – that was the reason why she killed herself. Now that I had lived in this body, the least I could do was to follow her wishes, and voiced them to this person sitting in front of me. 

Alexander narrowed his eyebrows. 

“Where you belong is for the emperor to decide. Speaking against the emperor’s order is a crime that can be severely punished. Are you ready for that?”

“No, of course not, your highness. I wouldn’t dare.” I lowered my gaze, tightening my grip on the dress.

“I shall hear no other words from this matter again.” He slammed the table and stood up, turning away from the dining table.

“B-Brother, you haven’t finished your food yet!” Elena hastily got up from her chair.

“I’m no longer hungry.” Alexander dropped his last words, before walking out of the room. 

“Brother!” 

Seeing Elena chasing after the crown prince, I shrugged and continued on with my breakfast. At least I tried, Seraphina. He seemed like a tough rock to crack, as expected. None of that mattered to me anyway. As soon as Alexander left the room, my appetite returned in a flash, and I quickly finished my delicious food in great joy. 

----------

After stuffing my belly with delicious food, I returned to my room. From what the maid said, Elena was also in her room, and we were free to do whatever we wanted for the rest of the day.

I remembered this part from the game as well - Elena spending the whole day in her room, crying about how she missed her grandparents in the countryside, and wept at how coldly her brother was treating her in the palace. I also remembered complaining about how the girl simply wasted her whole day on doing nothing but reminiscing, instead of trying to educate herself about the new world that she was just put in. 

"Now that I'm here, it's time to act out what I think is the best course of action, and like what a normal person - instead of the heroine - should do!" I fist bumped my reflection on the mirror with a triumphant look. 

With that, I headed to my first stop - the reading room.

------------
It did not take me long to find out where the reading room was with the instruction of the maids. The reading room was located on the third floor, its size as big as a local public library from my past life. 

"As expected of the royal palace. Even the reading room is fancy." 

I nodded at admiration of the grandiosity of the place. The heroine seldomly set foot in this place, according to the game, so as the player, I never got to see what the reading room was like. 

Speaking of which, there was no character named Seraphina Celandine in the original game. Could it be that I had added a new character to the original plot of the story? Or maybe Seraphina was too faded, she wasn't even mentioned in the story despite being there the whole time?

If that was the case, then I had nothing else to worry, but how to make the best out of this life as Seraphina while still having the chance to do so. One thing I regretted before leaving my past life was, after all, not spending more time having fun and enjoying what was given to me. 

My gaze glued to the bookshelves, where titles after titles listed next to each other. 

“First, I need to know more about the royal family bloodlines, in order to really figure out who Seraphina is. I also should look into the current news about how the country is doing as well.”

I was organizing my strategies, my head craning to one side in order to adjust read the titles, when I bumped into a hard as rock figure.

“Woah- “

I steadied myself and looked up.

Before me stood a silver-haired, tall figure. His hand clutching onto the thick book by his side. He was looking at me in surprise.

“I am terribly sorry. Are you alright?” 

I slightly lifted my dress and bowed at him with greetings. “No, it was my fault for not paying attention.”

I did not remember this person in the game. Perhaps it was because I was roaming around the palace out of my free will, I encountered characters that were not mentioned in the game. 

“I have never seen you before.” The other person smiled lightly, holding out his hand.

“Yes, my name is Seraphina. I will be living here from now on. It’s a pleasure to make your acquaintance.”

“No, the pleasure is mine. I am Zephyr, a priest in training. I come here often to borrow books from here.”

“I can understand. They do have quite a collection in here.” 

A priest in training. He must be good friend with the crown prince if he is allowed in the palace just for resources for his study.

“You said your name was Seraphina? Are you the rumored sister of Alexander the crown prince, who moved in just recently?” Zephyr’s eyes brightened up as if he had just discovered the hottest news of the day.

I shook my head. “Not quite. You might be referring to Lady Elena. I am nothing but an outsider to this place, who had been tangled with the thread of fate and ended up lingering here.”

“You speak in riddles.” The young priest laughed. “But I find that quite interesting. Are you saying that you have no connection to the royal family, despite living in a royal palace?”

“I am not denying that.”

I put up a façade and smiled politely at the other person, not wishing to reveal any other information. It was never a good idea to play all your cards during the first meeting. 

“Sir Zephyr, it seems that I must take my leave. I still have urgent matters to attend to.” Not waiting for the other party to speak any other word, I bowed slightly. 

“My bad. I shall not keep you any longer. May we meet each other soon.” 

After bidding farewell with the young priest at the reading room, I returned to my room to jot down some notes. Zephyr must have been a side character, since he seemed indifferent to the matters of the palace. He portraited little emotion, and kept the conversation well. 

“I should inquire more information about this character as I go on. It’s not good to be in the dark.”

I wonder if they sell personal data in this world. I would love to buy some, once I could get my hand on some allowance, or some sort of income. Relying on the host was never a good idea, since there was no telling when I would be kicked out. After all, I was but a visitor to this world, and the crown prince was under no obligation to keep sheltering me, no matter what he might insist.

During dinner that night, there were just the three of us again. Alexander sat at the head of the table, slicing his steak in an elegant way. Elena, on the other hand, could not stop fidgeting on her seat. Poor thing, she looked too nervous to eat. 

The silence went on for a while, and I happily enjoyed the meal already cooked for me, as I was starving from all the exploration I did around the palace.

“B-Brother, I have a request…”

Elena finally broke the silence. Given the information I heard from the maids, Elena arrived at this palace the same day as Seraphina did. So she was equally a newbie, and today was probably her first chance to actually speak to the crown prince, considering the chaos I brought during breakfast. The crown prince looked up from his plate, the annoyance clearly visible in his eyes.

“A lady should not speak while on the table.”

He paused, and glanced my way, as if to say ‘that applies also to you’. I swallowed my food and looked away, hoping to get out of any trouble.

“However, I will grant you this chance. Speak.”

I could almost hear the heroine gulping from the other end of the table. According to the main story, the heroine grew up in the countryside, like Seraphina, so she was not used to any etiquette in Alexander’s world. Seeing how she was disappointed being scolded was evident to confirm this fact.

“M-My apologies.” She stuttered, turning her gaze down. “M-May I ask for chopsticks? I’m not quite used to the silverware here…”

The crown prince sighed audibly, and waved for the maid nearby to provide Elena with a pair of chopsticks. 

“Would you be needing one too, Seraphina?” 

I was too emerged in observing the heroine and comparing how closely it resembles the storyline, that the sudden question addressed to “Seraphina” startled me.

“No, I’m quite good. Thank you for your concern.” 

I raised my hand to decline the offer from Alexander. He slightly raised his eyebrows as if he wanted to say something, but mumbled “if that’s what you want” instead.

I noticed the surprised look on Elena’s face as well, upon hearing my answer. It was not until I picked up my fork to eat my salad, that I realized what I did wrong. Seraphina was raised in the countryside, and therefore she was not supposed to be familiar with the etiquette on the table among aristocrat society. However, as a student studying in a foreign European country, it was common knowledge for me to be acquainted with these kinds of dishes and how to use the silverware. 

I had been too hungry, so I simply put no thought into it and naturally used different silverware to different dishes like I used to do in my past life!

Seeing the maids behind looking at me in awe, I silently cried inside. First day, and I had already dug my first grave.

“You seem awfully knowledgeable in the etiquette on the table.” The crown prince, who seemed to dislike talking at any time during meals, suddenly initiated the conversation again.

“No, this is…” I sweated. “I was just copying you, Your Highness. How could a country pumpkin like me know the proper way to act in such sophisticated manner?”

“Ah, I see.” Elena chimed in. “Since Seraphina sat so close to Brother, she was able to mirror your manner! That’s wonderful!”

I eyed the heroine. She sounded like she was jealous with me for sitting on the side, while she was seated at the other end of the table. However, she might not have known that her seat was the second to most importance seat on the table. Where the crown prince was sitting was the place of the host, and the other end, the second most important to the host, where Elena was sitting at. 

I was seated closer to Elena on the side, but judging from a physical distance, I was indeed closer to the crown prince.

“Elena, you will soon know dining etiquette as well. There is still time before you learn dine with others. Tomorrow, your teacher will arrive. You will learn how to dance, and have proper manner in the aristocrat society, as it is befitting for the role of my younger sister.”

“W-What?”

Elena was taken aback. 

“That also applies to you.”

I nearly choked on my food upon hearing those words.

“W-Why?” I put down my fork. It was not ideal for digesting food while hearing such shocking news. “I am not your younger sister, am I? Is there a need for me to learn such things?”

Alexander, however, did not like hearing opposition.

“You seem to have an awful lot of questions. Save it for your classes.”

Goodbye, my free days. Hello again, dreadful hours of studying.

After hearing this news, the food on the table no longer appealed to me. Alexander had also finished his food, despite being the one who talked the most among us. Elena was still struggling on her second course.

There was really no reason for me to learn etiquette that I could think of. Being the sister of the crown prince, meaning the crown princess, Elena would naturally have to learn the way of royalty. However, Seraphina was simply a side character, who probably would rarely, if not ever, appear in front of public. They could easily confine Seraphina to a room in the palace, shelter her from any other political parties who might want to take advantage of the royal bloodline inside her body. 

I suppose it was simply more economical for me to learn etiquette, since the teachers were coming for Elena anyway.

Satisfied with my made-up answer to cope with the hole of information in my brain, I nodded to myself.

“You two have already known where your rooms are. There is no need to go outside. If you need anything, a maid will bring it to you.”

“I’m sorry?” Elena asked, trying to comprehend the words spoken to her.

“Do not leave the palace. There is no reason for you to do so.”

Ah, here it was. The house arrest order from Alexander de Claus, knowing what danger awaited beyond the palace’s gate as soon as Elena set foot outside. It was a sound warning with valid reasons for such concern.

To this, Elena strongly opposed. She could not take in the fact that she would be confined here, within these four cold walls, not being able to freely go outside. Such a free and stubborn spirit she was, yet she could not sway Alexander’s decision.

“No wonder Seraphina killed herself.”

I mumbled to myself.

“Did you have something to say?” Alexander seemed to have caught that.

“Nothing, Your Highness. I am just thinking of how to spend my days ahead in boredom, since there will be barely anything for me to do.” I cocked my head to the side, saying that as a joke.

However, this took the crown prince off guard, and he did not say a word for a while after that, as if he was thinking hard about my statement.

“Indeed. Then you have my permission to go into the reading room. The books there might satisfy your boredom. Take a walk in the garden if you want. Even though, you will soon be busy with etiquette classes anyway.”

I had already wandered into the reading room, and I doubted the garden would provide that much of entertainment. Nonetheless, I kept the thought to myself and simply nodded in response, thanking him for the suggestion.


