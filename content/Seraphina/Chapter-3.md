---
title: "Chapter 3"
date: 2020-01-14T20:32:53-08:00
---

The next day, I went into the reading room as Alexander had suggested, only more officially since I had now received a formal permission to set foot in here. 

It was still early in the morning, and we had just finished breakfast. Breakfast passed by casually without any major incidents. Elena, the heroine, had offered to make tea for her brother the crown prince every breakfast. The crown prince nodded and agreed, much to the heroine’s surprise. She brightened up in response, and immediately rushed to get the tea ready.

I sat quietly throughout breakfast, already knowing what would have happened according to the game. I simply enjoyed the events that unfolded themselves before my eyes, without any intention to partake in any of them.

Returning to the current moment-

I was deep in thought, troubled by the variety of books I encountered. Politics, history, geography, economics – there were too many books that I found it difficult to find a starting place.

“Hmmmm… I really should look into politics first, since this is the royal palace after all. Knowing where I am living in would give me a bit of levelling… However, I am also curious about the economy around here. I need to make money after all. Ah, then geography would also be important, because I need to know what is essential to the people around here. Seraphina’s memory in the countryside provided me too little information – did this girl even care about what was happening around her at all?”

I continued to walk along the bookshelves while talking to myself to sort out my thoughts. 

“Not to mention, these books are thick and full of difficult words.” I opened a random book on the shelf, examining the content inside. “Unlike the books in my past life, these aren’t reader-friendly at all. There are hardly any pictures. Most of them are even harder to read than university textbooks.”

Not that university textbooks were interesting enough to keep my attention until the very last page.

“Ah, we meet again.”

I looked up from the densely-spaced page. Silver hair glittering in the sunshine, and golden eyes looking at me with amusement in them.

“Sir Zephyr. Good morning to you.”

“A good day to you as well, Lady Seraphina. What has invited you to the reading room?” He smiled and placed a greeting kiss on the back of my hand.

“Since I have been banned from going outside, I’m here to find some entertainment.”

“Is that so? Seems like the rumor that the crown prince favoring his relatives is true after all.”

“Favoring?” I snickered. 

“Yes. Otherwise, why would he do so much as to hide such beautiful flower inside the palace, forbidding anyone outside to witness her beauty?”

His flirting caught me off guard. My cheek turned slowly into a pink color.

“I-I do not believe that is the reason.” I stuttered, finding myself unable to think straight.

“Hah.” Zephyr let out a laugh. “You are too adorable.”

“Please cease with your unnecessary flattery, Sir Zephyr. I don’t think that is quite appropriate of you to say such things.” 

“Ah, then please forgive me. I didn’t mean to offend you.” He retracted his laughter, but his eyes were still smirking. “As an apology, is there anything I can help you with? I have been in this reading room for quite some time, so I know where most books are.”

I thought about it, hesitating about asking help from such a stranger after the conversation. However, if I were to spend my whole morning here looking for the perfect book to read, I would not be able to finish reading any before the etiquette teacher comes.

“Then I suppose I shall take you up on your offer.” 

I put the book in my hand away. Zephyr beamed at that answer. We started to walk slowly, further into the bookshelves. 

“Wonderful! What kind of book are you looking for?”

“I’m looking into…” I stopped to consider my word choice. “… the royal family. I want to know who the people I am staying with are, and the names of those I will likely encounter in the future.”

Zephyr blinked at me; his eyes widen in surprise. 

“What’s the matter?”

“Nothing. I just thought that was straightforward of you, wanting to know about the enemy den right away.” He shifted his gaze away from my direction, so I could not make out his facial expression.

“… Should I have waited?” Not understanding what the problem was, I inquired. Was I prying to much? 

And did he just mention the royal family as my ‘enemy den’?

“Maybe?” He turned back. His fake smile was up again. “Who knows. Usually, a lady should not pay too much attention to those things, and would have enjoyed the luxury of this place instead.”

And wait to be slaughtered like a lamb without knowing who the butcher was? No, thank you.

“I have no intention to stay in here long. However, I have no intention to die before I can set myself free, either. In order to do that, knowledge is necessary. I don’t want to be trampled over, but I also don’t want to get on someone’s wrong side whom I should not have.”

Maybe it was because Zephyr was a side character whose name was not even mentioned in the original storyline that I felt so free expressing my thoughts to him. His facial expression rarely changed, but he attentively listened to my words.

“Very well. Lady Seraphina, your mother was the younger sister of the current emperor. She was originally engaged to her brother, who was not in the line of succession like the emperor. Since they were both members of the Imperial family, they were both considered prince and princess at that time. The former prince loved your mother, but she fell in love with a commoner in town when she was disguising herself to sneak out of the palace. The prince knew of this information and was in rage. He convinced the emperor, who was the crown prince back then, to declare war with the neighboring country, which resulted in all men of the commoner class to be enlisted in the army. The princess was distressed when she knew of this. She sneaked out at midnight to see her lover, and they spent the night together. The next morning, the army set out for war. My guess would be that you were the fruit of her relationship, Lady Seraphina. When it was known that your mother was pregnant with the child not of royal blood, the prince was furious and chased her out of the palace. Your mother gave birth to you in your father’s household. Three years later, the war ended. However, her lover did not return. He died while fighting on battle.”

Zephyr’s voice was quiet, but I could hear every word clearly. 

I never thought Seraphina’s mother would have gone through something so tragic. In Seraphina’s memory, her mother was crying almost every night, while gazing outside of the window. She was probably waiting for her loved one to return. 

I felt tears welling up from inside. My vision went blurry.

However, this was not much or my emotions, but more like of Seraphina and her love for both her mother and her father, who she never had the chance to meet.

“I’m sorry.” 

I heard Zephyr’s soft voice as he inched closer and gave me a handkerchief. 

“Thank you. Sorry, I can’t seem to stop my tears from falling…”

I took the handkerchief and wiped my tears away, but that only made more of them coming. 

That was a peculiar experience for me, as I never cried in front of others in my past life. Unlike me, Seraphina was a crybaby. Therefore, despite my consciousness screaming at her body to stop crying, everything seemed to be futile.

Hic, hic-

“You speak… as if you witnessed everything yourself. Hic. Did the private life of a former princess… hic… something that public tabloid published for everyone to know?”

I spoke through my tears, struggling with the conflict of my consciousness and my behavior.

“Well… The Church was there all along, to help the princess.” He lowered his voice. “I, as a priest in training, was much involved as well. After all …”

“What is going on in here?”

A stern voice cut Zephyr off. We both turned around to see who was the uninvited third party, who was then standing not too far from us, hands crossing in front of his chest. Jade green eyes stared at us, void of emotions.

Not completely void. More like all emotions were clouded and hidden away, leaving the person with a mask that no one else can see through.

“Greetings, Your Highness.” 

Zephyr made a bow at the entrance of the crown prince, Alexander de Claus.

Realizing what was going on, I hastily bowed as well.




