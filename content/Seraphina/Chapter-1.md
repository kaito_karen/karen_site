---
title: "Chapter 1"
date: 2020-01-03T23:32:53-08:00
---


“You’re not going this time as well? Really, you need a break sometimes!”

I shook my head at my friends who were getting ready to go out for karaoke. This was not the first time I turned down their invitation, and they were not too happy with it.

“I will make it up to you guys some other time!” I said with a sad hint in my voice, knowing that “other time” will never come. My class schedule was full as it was for a senior student in university. Juggling two part-time jobs at the same time did not help to alleviate the stress.

If only I could have some break during this semester, I annoyedly thought to myself, I wouldn’t have felt so worn out. Every little thing frustrated me lately – the kids at the restaurant where I worked at who never seemed to learn their manner, the parents who were too focused on their conversation to keep their children from running around the place like it was a public park, the coworkers who jumped at every opportunity during work hours to slack off no matter what. Yet there was no one around for me to complain to or to relieve the stress – such was the fate of a student studying away from home.

My only safe haven was otome game. Nothing felt better than being a heroine that everyone loved, no matter what you did. No one had any expectation of you, and you were free to decide your course of action, knowing there would be a happy ending at the end of the route. Such world was so much different from reality – where you tried, tried, and tried, and would never feel enough: a world of loneliness, a pursuit of certain happiness that seemed to stretch on for eternity.

Maybe it was due to fatigue that my eyelids got heavier as I drove home from work. It was midnight – as the restaurant closed down and the last customer walked out, the city fell into a slumber. I was by myself on the road, driving at the streetlights at a carefree speed as all the traffic lights did not even bother to change their colors anymore.

“So quiet.” I said to no one in particular, and turned up the radio. My mind was not on the road, but floating into a fantasy world, when the container truck made a sudden turn.

The glass in front of me shattered, followed by a loud explosion that vibrated in my eardrums.

I felt the airbag popped out, but in the back of my mind, something in me knew that it was too late. 

In my next moment of consciousness, I saw the flashing red and blue light from police cars surround the mess, which was probably my car. I was lying on the cold hard concrete road, among shards of glass that sparkled like stars in the sky.

I shut my eyelids, feeling my body getting heavier. Finally, a moment of rest. Finally, I would not have to think about school for a while, or any of the stressors life was bombarding me with anymore. 

----------

“…phina. Lady Seraphina!”

A soft yet stern voice awoke me, ending the peaceful darkness that had enveloped me.

Before my eyes was a stylish floral canopy, and I was lying in a room that was completely foreign to me. 

A girl in maid uniform stood by my bed, with a worried expression on her face.

“Oh no, my lady. You look horrible! Did something happen last night?”

I was almost startled by her words, thinking I must have been invited into some kind of scam that would bill me thousands for this kinky maid service. However, right at that moment, memories from elsewhere rushed in.

> *‘I would rather die than be confined in here!’*

Said the person in “my” memories as she scrambled through her suitcase, fishing out a bottle of white pills. She knocked the content into her mouth, swallowed them hastily with water before throwing herself on the bed and sobbing uncontrollably.

> *‘I’m sorry, Felix, for breaking my promise to you. May we meet again in Heaven.’*

Such was the flash in my mind, as I stared blankly at the maid.

Like an instinct, I jumped out of bed and headed to the big decorated mirror on the table.

Blond hair with big curly waves that ran own to her waist. Blue eyes that looked like a pair of sapphire, staring into the mirror in shock.

I waved my hand at the mirror, and made a face.

The perfectly synced reflection told me that the girl in the mirror was me. Her eyes reddened from crying nonstop, and her eyelids were a little puffy from all that.

It was not until then that I felt a sudden dizziness swept over me. My feet gave out and I slumped into the floor, one hand on the table trying to steady myself. That was a given, considering how many sleeping pills the owner of this body took the night before.

“My lady!” The maid hurried over and helped me up. “You must not move so suddenly after waking up! You haven’t eaten anything yesterday, either! Please let me help you get ready, so we can head down to breakfast! The crown prince must be waiting right now!”

“The crown prince?”

Wait, who?

But the maid didn’t waste any moment. She positioned me on the chair, and sprinted along to do my hair and makeup. She looked so focus that I dared not ask her anything else, feeling a sense of emergency in her actions.



